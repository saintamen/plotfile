package com.sda.plotting;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame{

    private JScrollPane scroll;
    private JPanel main;

    public Window() throws HeadlessException {
        scroll = new JScrollPane(new WindowPanel("data.txt"));
        scroll.setBounds(0, 0, 100000, 10000);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        setContentPane(scroll);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(600, 400));
        setVisible(true);
    }
}
