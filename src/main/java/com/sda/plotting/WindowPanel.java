package com.sda.plotting;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WindowPanel extends JPanel {
    private String path;
    private List<Point> points;

    public WindowPanel(String s) {
        this.path = s;
        points = new ArrayList<Point>();

        try(BufferedReader reader = new BufferedReader(new FileReader(path))){
            String line = null;
            while ((line = reader.readLine()) != null){
                String[] dane =line.split(";");
                points.add(new Point(Double.parseDouble(dane[0]), Double.parseDouble(dane[1])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setSize(new Dimension(10000, 1000));
        setPreferredSize(new Dimension(10000, 1000));
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.WHITE);
        g.fillRect(0,0, 10000, 1000);
        g2d.setColor(Color.RED);
        for(Point p : points) {
            g2d.fillOval((int)(p.x*1.0), 500 + (int)(p.y*1.0), 5, 5);
        }
    }

    private class Point {
        private double x, y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }
}
